from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
import csv
import time

# Initializing the Chromium driver
# downloaded from https://chromedriver.storage.googleapis.com/index.html?path=97.0.4692.71/
service = Service(executable_path="chromedriver.exe")
driver = webdriver.Chrome(service=service)
action = ActionChains(driver)

# Going to Google Images
driver.get('https://www.google.ca/imghp?hl=en&tab=ri&authuser=0&ogbl')

# opening the original dataset, adding encoding format for windows
with open('all_games.csv', encoding="utf8") as f:
    data = csv.reader(f, delimiter=',')
    count = 0
    # fields for the new dataset with the image url
    fields = ['name', 'platform', 'release_date', 'summary', 'img_url']
    # creating the new dataset, newline='' added to avoid an empty row, added encoding format for windows
    with open('newDataSet.csv', mode='w', newline='', encoding="utf8") as csvFile:
        # Writing the fields in the first row
        writer = csv.DictWriter(csvFile, fieldnames=fields)
        writer.writeheader()
        for file in data:
            if count == 0:
                # skips the first line
                count = count + 1
            else:
                # Locate the searchBar element and enter the query
                searchBar = driver.find_element(By.CSS_SELECTOR, '[name="q"]')
                # Query from the original field, it takes the columns ("name" "platform" cover)
                searchBar.send_keys(file[0] + " " + file[1] + " front cover")
                # Enter key pressed
                searchBar.send_keys(Keys.ENTER)

                # Looks for the first thumbnail and clicks it
                imgThumbnail = driver.find_element(By.XPATH, '//*[@id="islrg"]/div[1]/div[1]')
                imgThumbnail.click()

                # delay added to load the image element to avoid url errors
                time.sleep(1.5)
                img = driver.find_element(By.XPATH,
                                          '//*[@id="Sva75c"]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div[2]/div/a/img')

                # Writing the new row with the needed data
                writer.writerow({'name': file[0], 'platform': file[1], 'release_date': file[2], 'summary': file[3],
                                 'img_url': img.get_attribute('src')})

                # Clear the search bar
                searchBar = driver.find_element(By.CSS_SELECTOR, '[name="q"]')
                searchBar.clear()
